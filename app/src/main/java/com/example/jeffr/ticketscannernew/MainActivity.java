package com.example.jeffr.ticketscannernew;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    EditText nameEdit;
    TextView nameText;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameEdit = (EditText)findViewById(R.id.nameText);
        nameText = (TextView)findViewById(R.id.topName);

        // bundle to pass user name to different activities
        Bundle bundle = new Bundle();
        bundle.putString("userName", name);

        // button to start photo intent
        final Button scanButton = (Button) findViewById(R.id.scanTicketButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TakePicture.class);
                intent.putExtra("userName", name);
                startActivity(intent);
            }
        });

        // button to start picture list intent
        final Button buttonImages = (Button) findViewById(R.id.buttonImages);
        buttonImages.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, displayticket.class);
                intent.putExtra("userName", name);
                startActivity(intent);
            }
        });

        // button to submit your name
        final Button nameButton = (Button) findViewById(R.id.nameButton);
        nameButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String name = nameEdit.getText().toString();
                nameText.setText(name);
                setName(name);
            }
        });
    }
    public void setName(String name){
        this.name = name;
    }
}
