package com.example.jeffr.ticketscannernew;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jeffr on 2/20/2016.
 */
public class DataSource {

    SQLiteOpenHelper dbhelper;
    SQLiteDatabase database;

    private static final String[] allColumns = {
            MySQLiteHelper.COLUMN_TICKET_ID,
            MySQLiteHelper.COLUMN_TICKET_LOCATION};

    // logtag for debug purposes
    public static final String LOGTAG="JeffRotz";

    public DataSource(Context context) {
        dbhelper = new MySQLiteHelper(context);

    }

    // Open database
    public void open() {
        Log.i(LOGTAG, "Database opened");
        database = dbhelper.getWritableDatabase();
    }

    // Close database
    public void close() {
        Log.i(LOGTAG, "Database closed");
        dbhelper.close();
    }

    // Delete database
    public void delete(){
        database.delete(MySQLiteHelper.TABLE_TICKETS, null, null);
    }


    //create a new ticket object with ID and URL
    public Ticket create(Ticket ticket) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_TICKET_LOCATION, ticket.getUrl());
        long insertid = database.insert(MySQLiteHelper.TABLE_TICKETS, null, values);
        ticket.setId(insertid);
        return ticket;
    }


    //retrieve all saved data from database and insert into ArrayList
    public ArrayList<Ticket> findAll() {
        ArrayList<Ticket> tickets = new ArrayList<Ticket>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_TICKETS, allColumns,
                null, null, null, null, null);

                Log.i(LOGTAG, "Returned " + cursor.getCount() + " rows");
        if (cursor.getCount() > 0) {
            while(cursor.moveToNext()) {
                Ticket ticket = new Ticket();
                ticket.setId(cursor.getLong(cursor.getColumnIndex(MySQLiteHelper.COLUMN_TICKET_ID)));
                ticket.setUrl(cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_TICKET_LOCATION)));
                ticket.setDrawable(ticket.getUrl());
                tickets.add(ticket);

            }
        }
        return tickets;
    }


}
