package com.example.jeffr.ticketscannernew;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by jeffr on 2/20/2016.
 */
public class MySQLiteHelper extends SQLiteOpenHelper{

    // Database info
    private static final String DATABASE_NAME = "ticketDatabase.db";
    private static final int DATABASE_VERSION = 1;

    // Assignments
    public static final String TABLE_TICKETS = "tickets";
    public static final String COLUMN_TICKET_LOCATION = "ticket";
    public static final String COLUMN_TICKET_ID = "ticketId";


    // Creating the table
    private static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_TICKETS + " (" +
                    COLUMN_TICKET_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_TICKET_LOCATION + " TEXT " +
                    ");";


    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Execute the sql to create the table assignments
        db.execSQL(TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TICKETS);
        onCreate(db);
    }
}
