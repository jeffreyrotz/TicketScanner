package com.example.jeffr.ticketscannernew;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jeffr on 2/21/2016.
 */
public class TicketAdapter extends ArrayAdapter<Ticket> {

    private Context context;
    private ArrayList<Ticket> tickets;
    private int layoutResourceId;
    private LayoutInflater inflater;



    public TicketAdapter(Context context, int layoutResourceId, ArrayList<Ticket> tickets) {
        super(context, layoutResourceId, tickets);

        this.context = context;
        this.tickets = tickets;
        this.layoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        Holder mHolder;


        if (row == null) {
            inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
        }

        // display new Holder with title and image
        mHolder = new Holder();
        mHolder.txtTitle = (TextView) row.findViewById(R.id.urlView);
        mHolder.image  = (ImageView) row.findViewById(R.id.photo);

        // convert Url to drawable and assign to imageView
        String text = tickets.get(position).getUrl();
        Drawable image = tickets.get(position).getDrawable();

        mHolder.txtTitle.setText(text);
        mHolder.image.setImageDrawable(image);

        return row;
    }

    static class Holder{
        TextView txtTitle;
        ImageView image;
    }
}
