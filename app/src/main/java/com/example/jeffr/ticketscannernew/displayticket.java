package com.example.jeffr.ticketscannernew;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class displayticket extends AppCompatActivity {

    //list all taken photo's

    DataSource datasource;
    ArrayList<Ticket> tickets;
    TicketAdapter ticketAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_displayticket);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // retrieve the user name
        String value = getIntent().getExtras().getString("userName");

        TextView textView = (TextView)findViewById(R.id.listName);
        textView.setText(value);

        // open database
        datasource = new DataSource(this);
        datasource.open();

        // find all tickets
        tickets = datasource.findAll();

        ListView listView = (ListView) findViewById(R.id.listView);

        //call the main layout from xml
        RelativeLayout activityDisplay = (RelativeLayout)findViewById(R.id.displayTicket);

        // use ticket_row as row layout file
        View view = getLayoutInflater().inflate(R.layout.ticket_row,activityDisplay,false);

        //add the view to the main layout
        activityDisplay.addView(view);

        ticketAdapter = new TicketAdapter(this,R.layout.ticket_row, tickets);
        listView.setAdapter(ticketAdapter);

        final Button removeButton = (Button) findViewById(R.id.removeButton);
        removeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            datasource.delete();
            }
        });

        
    }

}
