package com.example.jeffr.ticketscannernew;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by jeffr on 2/20/2016.
 */
public class Ticket {

    // ticket class for displayticket

    public static final String LOGTAG="JeffRotz";
    private long id;
    private String url;
    private Drawable drawable;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl(){
        return url;
    }


    public void setDrawable(String url) {
        Drawable drawable = Drawable.createFromPath(url);
        this.drawable = drawable;
    }

    public Drawable getDrawable() {
        return drawable;
    }


    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return url;
    }



}